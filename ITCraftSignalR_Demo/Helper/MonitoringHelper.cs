﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITCraftSignalR_Demo.Models.Monitoring;
using ITCraftSignalR_Demo.Repositories;

namespace ITCraftSignalR_Demo.Helper
{
    public static class MonitoringHelper
    {
        public static MessagesMonitoring GetMonitoringData()
        {
           var data = MessagesRepository.GetMessages()
                .GroupBy(x => new DateTime(x.Date.Year, x.Date.Month, x.Date.Day, x.Date.Hour, x.Date.Minute, 0))
                .Select(g=> new {
                    Metric = g.Key.ToShortTimeString(),
                    Count = g.Count()
                })
                .ToList();

            MessagesMonitoring monitorData = new MessagesMonitoring()
            {
                Periods = data.Select(x => x.Metric).ToList(),
                CountsOfMessages = data.Select(x => x.Count).ToList()
            };

            return monitorData;
        }

    }
}