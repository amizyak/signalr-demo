﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITCraftSignalR_Demo.Models.Action;
using ITCraftSignalR_Demo.Models.User;

namespace ITCraftSignalR_Demo.Repositories
{
    public static class UserRepository
    {
        static List<UserModel> users = new List<UserModel>() { };

        public static Int32 Count
        {
            get
            {
                return users.Count;
            }
        }


        public static void AddUser(UserModel model)
        {
            users.Add(model);
        }

        public static String GetUserName(String connettionId)
        {
            return users.FirstOrDefault(x => x.ConnectionId == connettionId)?.Username;
        }

        public static List<UserModel> GetUsers()
        {
            return new List<UserModel>(users.ToList());
        }

        public static void DeleteUser(String connettionId)
        {
            users.RemoveAll(x => x.ConnectionId == connettionId);
        }
    }
}