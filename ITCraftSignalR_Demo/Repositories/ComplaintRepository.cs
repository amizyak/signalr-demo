﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITCraftSignalR_Demo.Models.Action;

namespace ITCraftSignalR_Demo.Repositories
{
    public static class ComplaintRepository
    {
       static List<ComplaintModel> complaints = new List<ComplaintModel>() { };

        public static void AddComplaint(ComplaintModel model)
        {
            complaints.Add(model);
        }

        public static List<ComplaintModel> GetComplaints()
        {
            return new List<ComplaintModel>(complaints.ToList());
        }
    }
}