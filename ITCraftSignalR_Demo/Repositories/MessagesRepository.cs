﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ITCraftSignalR_Demo.Models.Action;
using ITCraftSignalR_Demo.Models.User;

namespace ITCraftSignalR_Demo.Repositories
{
    public static class MessagesRepository
    {
        static List<MessageModel> _messages = new List<MessageModel>();

        public static void SaveMessage(MessageModel message)
        {
            _messages.Add(message);
        }

        public static List<MessageModel> GetMessages()
        {
            return _messages;
        }



        }
}