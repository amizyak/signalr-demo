﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITCraftSignalR_Demo.Models.Action
{
    public class ComplaintModel
    {
       public String Name { get; set; }
       public String Message { get; set; }
    }
}