﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITCraftSignalR_Demo.Models.User
{
    public class MessageModel
    {
        public String Username { get; set; }
        public String Message { get; set; }
        public DateTime Date { get; set; }
    }
}