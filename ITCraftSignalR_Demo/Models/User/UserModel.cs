﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITCraftSignalR_Demo.Models.User
{
    public class UserModel
    {
        public String ConnectionId { get; set; }
        public String Username { get; set; }
    }
}