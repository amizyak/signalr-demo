﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITCraftSignalR_Demo.Models.Monitoring
{
    public class MessagesMonitoring
    {
       public List<String> Periods { get; set; }
       public List<Int32> CountsOfMessages { get; set; }

    }
}