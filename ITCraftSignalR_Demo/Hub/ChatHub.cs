﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ITCraftSignalR_Demo.Repositories;
using Microsoft.AspNet.SignalR;
using ITCraftSignalR_Demo.Models.User;
using ITCraftSignalR_Demo.Helper;

namespace ITCraftSignalR_Demo
{
    public class ChatHub : Hub
    {
        public void SendMessage(String username, String message)
        {
            MessageModel messageModel = new MessageModel() { Username = username, Message = message, Date = DateTime.Now };
            MessagesRepository.SaveMessage(messageModel);

            Clients.All.broadcastMessage(messageModel.Username, messageModel.Message, messageModel.Date.ToShortTimeString());
        }
        
        public void UserConnected(string userName)
        {
            UserRepository.AddUser(new UserModel() { ConnectionId = Context.ConnectionId, Username = userName });

            Clients.AllExcept(Context.ConnectionId).onNewUserConnected(userName, UserRepository.Count);
            Clients.Caller.onConnected(Context.ConnectionId, UserRepository.Count);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            String userName = UserRepository.GetUserName(Context.ConnectionId);
            UserRepository.DeleteUser(Context.ConnectionId);

            Clients.AllExcept(Context.ConnectionId).onUserDisconnected(userName);
            return base.OnDisconnected(stopCalled);
        }
    }
}