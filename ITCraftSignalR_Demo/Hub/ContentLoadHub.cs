﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ITCraftSignalR_Demo.Repositories;
using Microsoft.AspNet.SignalR;
using ITCraftSignalR_Demo.Models.User;
using ITCraftSignalR_Demo.Helper;
using System.Threading;

namespace ITCraftSignalR_Demo
{
    public class ContentLoadHub : Hub
    {
        public string Section1()
        {
            Thread.Sleep(5000);
            return "DONE";
        }
        public string Section2()
        {
            Thread.Sleep(5000);
            return "DONE";
        }
        public string Section3()
        {
            Thread.Sleep(5000);
            return "DONE";
        }
        public string Section4()
        {
            Thread.Sleep(5000);
            return "DONE";
        }
        public string Section5()
        {
            Thread.Sleep(5000);
            return "DONE";
        }
        public string Section6()
        {
            Thread.Sleep(5000);
            return "DONE";
        }
        public string Section7()
        {
            Thread.Sleep(5000);
            return "DONE";
        }
        public string Section8()
        {
            Thread.Sleep(5000);
            return "DONE";
        }
        public string Section9()
        {
            Thread.Sleep(5000);
            return "DONE";
        }
    }
}