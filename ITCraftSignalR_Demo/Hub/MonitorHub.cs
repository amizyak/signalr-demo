﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ITCraftSignalR_Demo.Repositories;
using Microsoft.AspNet.SignalR;
using ITCraftSignalR_Demo.Models.User;
using ITCraftSignalR_Demo.Helper;

namespace ITCraftSignalR_Demo
{
    public class MonitorHub : Hub
    {
        public void GetChatStatistic()
        {
            Clients.All.sendStatistic(MonitoringHelper.GetMonitoringData());
        }
    }
}