﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ITCraftSignalR_Demo.Models.Action;
using ITCraftSignalR_Demo.Repositories;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;

namespace ITCraftSignalR_Demo.Controllers
{
    [RoutePrefix("api/Complaint")]
    public class ComplaintController : ApiController
    {
        public List<ComplaintModel> Get()
        {
            return ComplaintRepository.GetComplaints();
        }

        [HttpGet]
        [Route("addComplaint")]
        public void AddComplaint([FromUri]ComplaintModel complaint)
        {
            ComplaintRepository.AddComplaint(complaint);

            var _context = GlobalHost.ConnectionManager.GetHubContext<ComplaintHub>();
            _context.Clients.All.addedComplaint(complaint);
        }
    }
}
