﻿/// <reference path="wwwroot/lib/signalr/jquery.signalR.js" />
/// <reference path="wwwroot/lib/signalr/jquery.signalR.js" />
module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.initConfig({
        clean: {
            build: ['build/**'],
            release: ['build/*']
        },

        uglify: {
            all: {
                options: {
                    sourceMap: true
                },
                files: { 'build/js/app.min.js': ['src/js/**/*.js'] }
            },
            prod: {
                files: { 'build/js/app.min.js': ['src/js/**/*.js'] }
            }
        },

        watch: {
            all: {
                files: ['src/js/**/*.js', 'src/style/**/*.css'],
                tasks: ['uglify:all']
            },
            prod: {
                files: ['src/js/**/*.js', 'src/style/**/*.css'],
                tasks: ['uglify:prod', 'ngtemplates:app', 'cssmin:prod']
            }
        },

        cssmin: {
            all: {
                options: {
                    sourceMap: true
                },
                files: {
                    'build/css/main.css': ['build/css/main.css']
                }
            },
            prod: {
                files: {
                    'build/css/main.css': ['build/css/main.css']
                }
            }
        },
        copy: {
            build: {
                files: [
                    {
                        expand: true, cwd: 'wwwroot/lib/',
                        src: ['**/jquery.min.js',
                            '**/spin.min.js',
                            '**/angular.min.js',
                            '**/angular.min.js.map',
                            '**/angular-messages.min.js',
                            '**/angular-ui-router.min.js',
                            '**/angular-resource.min.js',
                            '**/bootstrap-sass/assets/javascripts/bootstrap.min.js',
                            '**/ui-grid.min.js',
                            '**/ui-bootstrap.min.js',
                            '**/ui-bootstrap-tpls.min.js',
                            '**/angular-animate.min.js',
                            '**/angular-animate.min.js.map',
                            '**/restangular.min.js',
                            '**/jquery.signalR.js',
                            '**/Chart.min.js',
                            '**/Chart.bundle.min.js',
                            '**/angular-chart.js',
                            '**/angular-chart.min.js',
                            '**/angular-chart.min.js.map'
                        ],
                        dest: 'build/libs/js/',
                        filter: 'isFile', flatten: true
                    },
                      { expand: true, cwd: 'src/css/', src: '**/*.css', dest: 'build/css/', filter: 'isFile', flatten: true }
                ]
            }
        }
    });

    grunt.registerTask('debug', ['clean:build', 'uglify:all', 'copy', 'watch:all']);
    //grunt.registerTask('production', ['clean:release', 'ngtemplates', 'sass', 'uglify:prod', 'cssmin:prod', 'copy', 'watch:prod']);

};