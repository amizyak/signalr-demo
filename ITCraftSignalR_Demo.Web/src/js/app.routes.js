﻿'use strict';

signalRApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $stateProvider.state('action', {
        url: '/action',
        templateUrl: 'src/js/components/action/action.html',
        controller: 'actionController',
        data: {
        }
    })
    .state('complaints', {
        url: '/complaints',
        templateUrl: 'src/js/components/complaints/complaints.html',
        controller: 'complaintsController',
        data: {
        }
    })
    .state('chat', {
        url: '/chat',
        templateUrl: 'src/js/components/chat/chat.html',
        controller: 'chatController',
        data: {
        }
    })
    .state('monitor', {
        url: '/monitor',
        templateUrl: 'src/js/components/monitor/monitor.html',
        controller: 'monitorController',
        data: {
        }
    })
    .state('contentload', {
        url: '/contentload',
        templateUrl: 'src/js/components/contentload/contentload.html',
        controller: 'contentloadController',
        data: {
        }
    })
    ;

    $urlRouterProvider.otherwise('/action');
}]);
