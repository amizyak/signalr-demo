﻿(function () {
    'use strict';

    angular
        .module('signalRApp')
        .factory('interceptor', interceptor);

    interceptor.$inject = ['$rootScope', '$q', '$injector'];

    function interceptor($rootScope, $q, $injector) {
        return {
            request: function (config) {
               

                return config;
            },
            response: function (response) {
                return response;
            },
            responseError: function (rejection) {
                var deferred = $q.defer();

                if (rejection.status == 401) {
                    if (!rejection.config.ignore401intercept) {
                        var authService = $injector.get('authService');
                        var httpBuffer = $injector.get('httpBuffer');

                        httpBuffer.setHttpConfig(deferred, rejection.config);

                        authService.refreshToken()
                                .then(function () {
                                    httpBuffer.retryAll()
                                        .then(function () {
                                            deferred.resolve();
                                        }).catch(function (error) {
                                            deferred.reject(error);
                                        });
                                }).catch(function (error) {
                                    deferred.reject(error);
                                });

                        console.clear();
                        return deferred.promise;
                    } else {
                        console.clear();
                        return $q.reject(rejection);
                    }
                }

                if (rejection.status == 400) {
                    if (rejection.data.hasOwnProperty('ModelState')) {
                        for (var name in rejection.data.ModelState) {
                            if (name === '') {
                                $rootScope.error(rejection.data.ModelState[name][0]);
                            } else {
                                for (var i = 0; i < rejection.data.ModelState[name].length; i++) {
                                    $rootScope.addErrorMessage(name, rejection.data.ModelState[name][i]);
                                }
                            }
                        }
                    } else if (rejection.data.hasOwnProperty('error_description')) {
                        $rootScope.addErrorMessage('error_description', rejection.data.error_description);
                    } else if (rejection.data.hasOwnProperty('Message')) {
                        $rootScope.error(rejection.data.Message);
                    }

                    console.clear();
                    return $q.reject(rejection);
                }
            }
        }
    }
})();