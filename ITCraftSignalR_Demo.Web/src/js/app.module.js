﻿
var signalRApp = angular.module('signalRApp',
    [
        'ui.router',
        'ngResource',
        'chart.js'
    ]);


signalRApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}]);

signalRApp.config(['$compileProvider', '$httpProvider',
    function ($compileProvider, $httpProvider) {

        $compileProvider.debugInfoEnabled(false);
        $httpProvider.interceptors.push('interceptor');
        

    }]).run(['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toStateParams) {
                $rootScope.toState = toState;
                $rootScope.toStateParams = toStateParams;
            });
    }
    ]);

signalRApp.controller('baseController', ['$scope', '$rootScope', '$state',
    function ($scope, $rootScope, $state) {

        var errorMessages = [];
        $rootScope.pageTitle = '';

        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            if (toState.data && toState.data.pageTitle) {
                $rootScope.title = 'SignalR - ' + toState.data.pageTitle;
                $rootScope.pageTitle = toState.data.pageTitle;
            }
        });
    }]);
       