﻿(function () {

    angular.module('signalRApp').controller('chatController', ['$scope', function ($scope) {

        $scope.chatHub = null;
        $scope.chatStarted = false;
        $scope.userId = '';

        $scope.chatMessage = {
            message: '',
            username: ''
        }
        $scope.messages = [];
        $scope.usersCount;
        
        
        $scope.chatHub = $.connection.chatHub;

        $scope.chatHub.client.onConnected = function (id, usersCount) {
            var newMessage = {
                username: "You has been connected to ITC Chat!"
            }
            $scope.userId = id;
            $scope.usersCount = usersCount;
            $scope.messages.push(newMessage);
            $scope.$apply();
        };
        $scope.chatHub.client.onNewUserConnected = function (userName, usersCount) {

            var newMessage = {
                username: userName + " has been connected!"
            }
            $scope.usersCount = usersCount;
            $scope.messages.push(newMessage);
            $scope.$apply();
        };
        $scope.chatHub.client.onUserDisconnected = function (userName) {

            var newMessage = {
                username: userName + " has left!"
            }
            $scope.messages.push(newMessage);
            $scope.$apply();
        };
        $scope.chatHub.client.broadcastMessage = function (userName, message, time) {
            
            var newMessage = {
                username: userName,
                message: message,
                time: "("+ time + ")"
            }
            $scope.messages.push(newMessage);
            $scope.$apply();
        };

        $.connection.hub.start().done(function () {
            $scope.newMessage = function () {
                $scope.chatHub.server.sendMessage($scope.chatMessage.username, $scope.chatMessage.message);
                $scope.chatMessage.message = '';
            }
        }).fail(function () { });
        
        $scope.startChat = function () {
            $scope.chatStarted = true;
            $scope.chatHub.server.userConnected($scope.chatMessage.username);
           
        }
        
    }])
    })();