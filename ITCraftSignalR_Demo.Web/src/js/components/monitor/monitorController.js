﻿(function () {

    angular.module('signalRApp').controller('monitorController', ['$scope', function ($scope) {

        $scope.monitorHub = null;
        $scope.monitorHub = $.connection.monitorHub;

        $scope.monitorHub.client.sendStatistic = function (monitorData) {
            $scope.labels = monitorData.Periods;
            $scope.data = [
                monitorData.CountsOfMessages
            ];
            $scope.$apply();
        };
        $.connection.hub.start().done(function () {
            setInterval(function () {
                $scope.monitorHub.server.getChatStatistic();
            }, 2000);
        }).fail(function () { });


        //***
        $scope.series = ['Messages'];
        $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
        $scope.options = {
            scales: {
                yAxes: [
                  {
                      id: 'y-axis-1',
                      type: 'linear',
                      display: true,
                      position: 'left',
                      ticks: {
                          beginAtZero: true,
                          userCallback: function (label, index, labels) {
                              // when the floored value is the same as the value we have a whole number
                              if (Math.floor(label) === label) {
                                  return label;
                              }
                          }
                      }
                  }
                ]
            }
        };
//***

    }])
    })();