﻿(function () {
    'use strict';

    angular
        .module('signalRApp')
        .controller('complaintsController', complaintsController);

    complaintsController.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'complaintService', 'actionAlert'];

    function complaintsController($rootScope, $scope, $state, $stateParams, complaintService, actionAlert) {
        $scope.complaints = [];

        actionAlert.on('addedComplaint', function (complaint) {
             $scope.complaints.push(complaint);
         });

         $scope.getData = function() {
             $scope.complaints = complaintService.getComplaints().query();
         }

         $scope.getData();
    }
    })();