﻿(function () {

    angular.module('signalRApp').controller('contentloadController', ['$scope', '$resource', function ($scope, $resource) {

        $scope.contentLoadHub = null;

        $scope.contentLoadHub = $.connection.contentLoadHub;

        $.connection.hub.start().done(function () {
            $scope.loadSignalRSection1 = function () {
                $scope.contentLoadHub.server.section1().done(function (data) {
                    $scope.isloaded1 = true;
                    $scope.$apply();
                });
            }
            $scope.loadSignalRSection2 = function () {
                $scope.contentLoadHub.server.section2().done(function (data) {
                    $scope.isloaded2 = true;
                    $scope.$apply();
                });
            }
            $scope.loadSignalRSection3 = function () {
                $scope.contentLoadHub.server.section3().done(function (data) {
                    $scope.isloaded3 = true;
                    $scope.$apply();
                });
            }
            $scope.loadSignalRSection4 = function () {
                $scope.contentLoadHub.server.section4().done(function (data) {
                    $scope.isloaded4 = true;
                    $scope.$apply();
                });
            }
            $scope.loadSignalRSection5 = function () {
                $scope.contentLoadHub.server.section5().done(function (data) {
                    $scope.isloaded5 = true;
                    $scope.$apply();
                });
            }
            $scope.loadSignalRSection6 = function () {
                $scope.contentLoadHub.server.section6().done(function (data) {
                    $scope.isloaded6 = true;
                    $scope.$apply();
                });
            }
            $scope.loadSignalRSection7 = function () {
                $scope.contentLoadHub.server.section7().done(function (data) {
                    $scope.isloaded7 = true;
                    $scope.$apply();
                });
            }
            $scope.loadSignalRSection8 = function () {
                $scope.contentLoadHub.server.section8().done(function (data) {
                    $scope.isloaded8 = true;
                    $scope.$apply();
                });
            }
            $scope.loadSignalRSection9 = function () {
                $scope.contentLoadHub.server.section9().done(function (data) {
                    $scope.isloaded9 = true;
                    $scope.$apply();
                });
            }
        });


        $scope.getSection1 = function () {
            return $resource('/SignalR/api/loadContent/getSection1').get(function() {
                $scope.isloaded1 = true;
            });
        }
        $scope.getSection2 = function () {
            return $resource('/SignalR/api/loadContent/getSection2').get(function () {
                $scope.isloaded2 = true;
            });
        }
        $scope.getSection3 = function () {
            return $resource('/SignalR/api/loadContent/getSection3').get(function () {
                $scope.isloaded3 = true;
            });
        }
        $scope.getSection4 = function () {
            return $resource('/SignalR/api/loadContent/getSection4').get(function () {
                $scope.isloaded4 = true;
            });
        }
        $scope.getSection5 = function () {
            return $resource('/SignalR/api/loadContent/getSection5').get(function () {
                $scope.isloaded5 = true;
            });
        }
        $scope.getSection6 = function () {
            return $resource('/SignalR/api/loadContent/getSection6').get(function () {
                $scope.isloaded6 = true;
            });
        }
        $scope.getSection7 = function () {
            return $resource('/SignalR/api/loadContent/getSection7').get(function () {
                $scope.isloaded7 = true;
            });
        }
        $scope.getSection8 = function () {
            return $resource('/SignalR/api/loadContent/getSection8').get(function () {
                $scope.isloaded8 = true;
            });
        }
        $scope.getSection9 = function () {
            return $resource('/SignalR/api/loadContent/getSection9').get(function () {
                $scope.isloaded9 = true;
            });
        }

        $scope.loadAjaxData = function () {
            $scope.getSection1();
            $scope.getSection2();
            $scope.getSection3();
            $scope.getSection4();
            $scope.getSection5();
            $scope.getSection6();
            $scope.getSection7();
            $scope.getSection8();
            $scope.getSection9();
        }

        $scope.loadSignalRData = function () {
            $scope.loadSignalRSection1();
            $scope.loadSignalRSection2();
            $scope.loadSignalRSection3();
            $scope.loadSignalRSection4();
            $scope.loadSignalRSection5();
            $scope.loadSignalRSection6();
            $scope.loadSignalRSection7();
            $scope.loadSignalRSection8();
            $scope.loadSignalRSection9();
        }

    }])
    })();