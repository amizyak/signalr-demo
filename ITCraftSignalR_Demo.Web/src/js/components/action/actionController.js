﻿(function () {
    'use strict';

    angular.module('signalRApp').controller('actionController', actionController);
    actionController.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'complaintService'];

    function actionController($rootScope, $scope, $state, $stateParams, complaintService) {

        $scope.complaint;

        $scope.addComplaint = function (complaint) {
            complaintService.addComplaint(complaint).query();
        };
    }
    })();