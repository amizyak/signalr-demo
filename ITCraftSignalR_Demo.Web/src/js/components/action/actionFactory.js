﻿angular
       .module('signalRApp')
       .factory('complaintService', complaintService)
       .factory('actionAlert', actionAlert);

complaintService.$inject = ['$resource'];
function complaintService($resource) {

    return {
        addComplaint: function (complaint) {
            return $resource('/SignalR/api/complaint/addComplaint', complaint);
        },
        getComplaints: function () {
            return $resource('/SignalR/api/complaint/get');
        }
    }
}

actionAlert.$inject = ['$rootScope'];
function actionAlert($rootScope) {
    return {
        on: function (eventName, callback) {
            var connection = $.hubConnection('/SignalR/signalr', { useDefaultPath: false });
            var ticketHubProxy = connection.createHubProxy('complaintHub');

            ticketHubProxy.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(ticketHubProxy, args);
                });
            });
            connection.start().done(function () { });

        }
    };
}

        
